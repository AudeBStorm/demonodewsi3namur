const http = require("http");

console.log("Demo Node Web Server");

const usersTab = [
    { id : 1, lastname : 'Beurive',  firstname : 'Aude'},
    { id : 2, lastname : 'Strimelle',  firstname : 'Aurélien'}
];

// http.createServer((req, res) => {}).listen(8080);
//Pour créer un server, on doit appeler la méthode createServer du module http
//Cette méthode attend un callback, qui prend en premier paramètre, la requête et en deuxième, la réponse
const server = http.createServer((req, res) => {
    //L'object req -> request, contient toutes les infos sur la requête
    //console.log(req);
    //Dans req.url nous optenons, l'url que l'utilisateur a tapé
    console.log(req.url);

    //Pour gérer les routes
    //Pour traiter les différentes url, on va devoir vérifier à la main, si req.url
    //vaut des valeurs en particulier
    if(req.url === '/users') {
        const data = { users : usersTab };

        res.writeHead(200, { 
            'Content-Type' : 'application/json'
         });

        res.write(JSON.stringify(data));
        res.end();
    }
    //Pour gérer les routes /users/id
    else if(/^\/users\/[0-9]+$/.test(req.url)){
        // let regEx = /^\/users\/[0-9]+$/;
        // regEx.test('chaine à tester');
        const id = req.url.split('/')[2];
        const data = {
            user : usersTab.find(user => user.id == id)
        }
        
        res.writeHead(200, { 
            'Content-Type' : 'application/json'
         });

        res.write(JSON.stringify(data));
        res.end();
     }
     else if(req.url === '/rutabaga')
     {
        const data = {
            msg : 'Oui.'
        }

        res.writeHead(200, { 
            'Content-Type' : 'application/json'
         });

        res.write(JSON.stringify(data));
        res.end();
     }

});

//Pour lancer le serveur sur le port 8080
server.listen(8080, () => {
    console.log("Server up on port 8080");
})